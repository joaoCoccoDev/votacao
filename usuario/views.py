from django.shortcuts import render , redirect
from django.contrib import messages 
from django.contrib.auth import login
from .forms import UserCreationForm
from django.views.decorators.csrf import csrf_protect 


# Create your views here.

@csrf_protect 
def register(request):

    if(request.method == 'POST'):
        form = UserCreationForm(request.POST or None)
        if form.is_valid():
            user = form.save()
            login(request , user )
            messages.success(request, 'Usuario registered successfully')
            return redirect('/')

    form = UserCreationForm()
    context = {
        'form' : form
    }

    return render( request, 'register.html', context )

