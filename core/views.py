from django.shortcuts import render

from django.shortcuts import render , redirect
from django.contrib import messages 
from .models import Postagem  , VotoPostagemEleitor
from .forms import PostagemModelForm 
from usuario.models import User

# Create your views here.

def index( request ):
    context = {
        'postagens' : Postagem.objects.all(),   
    }   

    return render(request , 'index.html', context)

def vote( request , id ):

    user = User.objects.get(id=request.user.id)


    # if not user.get_isFalse():
    #     messages.warning(request, "Para que voce vote é preciso validar sua conta")
    # else:


    if user.voteValid():
        messages.error(request,'Voce ja votou 3 vezes')
    else:
        user.decrementvote()

        postagem = Postagem.objects.get(id=id)
        voto = VotoPostagemEleitor.objects.create(eleitor=request.user, postagem=postagem,  voted=False ) 
        
        voto.save()
        postagem.vote() 

        messages.success(request , "Voce votou na foto: " + postagem.titulo)
        
    return redirect('index')

def postagem( request ):

    if str(request.method) == 'POST':
        form = PostagemModelForm( request.POST, request.FILES)
        if form.is_valid():
            form.instance.autor = request.user
            form.save()
            messages.success(request , "Postado com sucesso")
            form = PostagemModelForm()
        else:
            messages.error(request , "Erro ao cadastrar")

    else:
        form = PostagemModelForm()

    context = {
        'form' : form , 
    }

    return render(request , 'postagem.html' , context)


def auth( request ):
    context = {
        'user':request.user.numero
    }
    return render( request , 'auth.html' , context ) 

from twilio.rest import Client 
import random

def authTeste( request ):


    TWILLIO_PHONE = 'seu twiliophone'
    account_sid = 'accont sid do twilio' 
    auth_token = 'auth token twilio' 
    USERPHONE = 'whatsapp:+'+request.POST.get('number')
    client = Client(account_sid, auth_token) 

    def gethash(self):
        return random.randint(100000,999999) 
    
    message = client.messages.create( 
                                from_=TWILLIO_PHONE,  
                                body=f'Olá, Para se autenticar na aplicação basta digitar o numero : {gethash}  no chat' ,      
                                to=USERPHONE,
                            ) 

    
    print(message)
    return render( request , 'auth.html'  )
