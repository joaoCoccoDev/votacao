# Generated by Django 4.1.1 on 2022-09-06 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='postagem',
            name='userHasVoted',
            field=models.BooleanField(default=False),
        ),
    ]
