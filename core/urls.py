from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import index , vote , postagem , auth , authTeste

urlpatterns = [ 
    path( '', login_required(index), name='index'),
    path('vote/<int:id>/' , login_required(vote) , name="voto" ) ,
    path('postagem/' ,login_required(postagem) , name="postagem"),
    path('auth/' ,login_required(auth) , name="auth"),
    path('auth/teste' , login_required(authTeste) , name="twilio"),
]