from django.db import models

from stdimage.models import StdImageField

from usuario.models import User
# Create your models here.

class Postagem( models.Model ):

    titulo = models.CharField("Título" , max_length=255 )
    descricao = models.CharField("Descrição" , max_length=1000)
    imagem = StdImageField('Imagem' , upload_to="postagem", variations={'thumb':(124,124)})
    votos  = models.IntegerField(default=0)
    autor = models.ForeignKey(User, on_delete=models.CASCADE , null = True)
    

    def __str__(self):
        return self.titulo

    def vote(self,*args, **kwargs): 
        self.votos += 1 
        return super(Postagem , self).save(*args, **kwargs)

class VotoPostagemEleitor( models.Model ):
    postagem = models.ForeignKey(Postagem , on_delete=models.CASCADE , null = True)
    eleitor = models.ForeignKey(User , on_delete = models.CASCADE, null = True ) 
    voted  = models.BooleanField(default=True) 
    